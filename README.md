# IG BvC - PoC II - AP 1.3 IAM

IG BvC - PoC II - AP 1.3 Identity and Access Management (IAM)


Die Wesentlichen Arbeitsergebnisse lassen sich wie folgt festhalten:
-	Es wurden UseCases beschrieben, aus denen sich die Kommunikationsflüsse und Voraussetzungen ableiten lassen, die für ein föderiertes Netzwerk von Identity-Providern zwingend erforderlich sind.
-	Es wurde über mehrere Iterationsschritte ein erstes Modell einer föderierten Identitäts- und Servicelandschaft für eine mögliche Implementierung der DVS entwickelt.
-	Da der OpenID Connect-Standard verwendet werden soll, wurden erste Vorschläge für  sogenannte Scopes und Claims erarbeitet, die beschreiben, welche Informationen zu Identitäten zwischen den Systemen in welcher Form übermittelt werden.
-	Ein struktureller Abgleich mit der in Österreich genutzten Infrastruktur zum Management von Identitäten in der Öffentlichen Verwaltung wurde durchgeführt.

Die im November 2022 erreichten Ergebnisse wurden an den MVP-IAM zur DVS übergeben, den die govdigital im Auftrag des BMI durchgeführt hat. Die Arbeiten am Arbeitspaket wurden damit beendet.



